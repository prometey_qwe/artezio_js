// alert("It works")


// // задание 1.1
// for(var i=10; i <=20; i++){
//     console.log(i*i);
// }

// //задание 1.2
// var sum=0;
// for(var i=10; i<=20; i++){
//     sum+=i;
// }
// console.log("\nsum is "+sum)


function buttonClick(){
    var sign1 = document.getElementById('sum');
    var sign2 = document.getElementById('mult');
    var sign3 = document.getElementById('simple');
    var x1 = document.getElementById('x1').value;
    var x2 = document.getElementById('x2').value;

    if(x1!="" && x2!=""){
        x1 = parseInt(x1)
        x2 = parseInt(x2)
        if(Number.isNaN(x1) || Number.isNaN(x2)){
            alert("В полях х1 и х2 должны быть числовые значения.");
        }else{
            
            var resultDiv = document.getElementById('result');
            resultDiv.innerText="";
            if(sign1.checked){
                var sum = 0;
                for(var i = x1; i <=x2; i++){
                    sum+=i;
                }
                resultDiv.append("Сумма чисел от х1 до х2 = " + sum);
            }else if(sign2.checked){
                var mult = 1;
                for(var i = x1; i <=x2; i++){
                    mult*=i;
                }
                resultDiv.append("Произведение чисел от х1 до х2 = " + mult);
            }else if(sign3.checked){
                var arr = [];
                for(var i = x1, j=0; i <= x2; i++,j++)
                    arr[j] = i;
                sieve(arr, 2, arr[arr.length-1], arr.length);
                var resultStr="";
                for(var i=0; i < arr.length; i++)
                    if(arr[i]>0) resultStr+=arr[i]+" ";
                resultDiv.append("Простые числа от х1 до х2 = " + resultStr);
            }
        }
    }
    else{
        alert("Поля х1 и х2 должны быть заполнены.")
    }
}
//задание 4
function clearBtn(){
    var x1 = document.getElementById('x1');
    var x2 = document.getElementById('x2');
    x1.value="";
    x2.value="";   

}

//задание 6
function sieve(arr, p, max, size){
    for(var num_p = 2; num_p*p <= max; num_p++){
        for(var i=0; i < size; i++){
            if(arr[i] == num_p*p){
                arr[i]=-1;
                break;
            }
        }
    }

    var next_num;
    for(next_num=0; next_num < size; next_num++){
        if(arr[next_num] > p && arr[next_num] > 0){
            break;
        }
    }
    if(arr[next_num]*2<max){
        sieve(arr, arr[next_num], max, size);
    }
}

